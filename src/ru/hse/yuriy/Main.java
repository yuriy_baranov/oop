package ru.hse.yuriy;


import java.io.*;
import java.lang.reflect.Array;


class GetOptParser {
    private InputStream in = System.in;
    private OutputStream out = System.out;

    GetOptParser(String[] args) throws FileNotFoundException {
        if (args.length >= 1 && !args[0].startsWith("-")) {
            in = new FileInputStream(args[0]);
        }
        if (args.length >= 2 && !args[1].startsWith("-")) {
            out = new FileOutputStream(args[1]);
        }
    }

    public InputStream getInputStream() {
        return in;
    }

    public OutputStream getOutputStream() {
        return out;
    }
}

class ImageProcessor {
    private InputStream in;
    private OutputStream out;

    String s;

    byte[] b = new byte[1024];
    byte[] y, t;
    int A, k, a, r, i, u;
    int n;
    String d = "P%d\n%d\40%d\n%d\n\00wb+";

    ImageProcessor(InputStream in, OutputStream out, int argc) {
        this.in = in;
        this.out = out;
        n = argc;
    }

    void prepare() throws Exception {
        y = ("yuriyurarararayuruyuri*daijiken**akkari~n**" +
            "/y*u*k/riin<ty(uyr)g,aur,arr[a1r2a82*y2*/u*r{uyu}riOcyurhiyua**rrar+*arayra*=" +
            "yuruyurwiyuriyurara'rariayuruyuriyuriyu>rarararayuruy9uriyu3riyurar_aBrMaPrOaWy^?" +
            "*]/f]`;hvroai<dp/f*i*s/<ii(f)a{tpguat<cahfaurh(+uf)a;f}vivn+tf/g*`*w/jmaa+i`ni(" +
            "i+k[>+b+i>++b++>l[rb").getBytes("US-ASCII");
        t = ("~hktrvg~dmG*eoa+%squ#l2" +
            ":(wn\"1l))v?wM353{/Y;lgcGp`vedllwudvOK`cct~[|ju {stkjalor(stwvne\"gt\"yogYURUYURI").getBytes("US-ASCII");
        int i;
        for (i = 0; i < 101; i++) {
            y[i * 2] ^= t[i] ^ y[i * 2 + 1] ^ 4;
        }
        for (a = k = u = 0; y[u] != 0; u = 2 + u) {
            y[k++] = y[u];
        }
    }

    void process() throws IOException {
        if ((a = in.read(b, 0, 1024)) > 2 && b[0] == 'P' && in.read() && !(k - 6 && k - 5) && r == 255) {
            u = A;
            if (n > 3) {
                u++;
                i++;
            }
            fprintf(q, d, k, u >> 1, i >> 1, r);
            u = k - 5 ? 8 : 4;
            k = 3;
        } else {
            (u) = +(n + 14 > 17) ? 8 / 4 : 8 * 5 / 4;
        }

        for (r = i = 0; ;) {
            u *= 6;
            u += (n > 3 ? 1 : 0);
            if (y[u] & 01)
                fputc(1 * (r), q);
            if (y[u] & 16)
                k = A;
            if (y[u] & 2)
                k--;
            if (i == a) {
                i = a = (u) * 11 & 255;
                if (1 && 0 >= (a = fread(b, 1, 1024, p)) && ")]i>(w)-;} {                                         /i-f-(-m--M1-0.)<{"[8] == 59)
                    break;
                i = 0;
            }
            r = b[i++];
            u += (+8 &* (y + u)) ? (10 - r ? 4 : 2) : (y[u] & 4) ? (k ? 2 : 4) : 2;
            u = y[u] - (int)'`';
        }
    }
}

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println(args.length);

        FileOutputStream s = new FileOutputStream("1.txt");
        s.write(d.getBytes());
        s.flush();
        s.close();
    }
}
